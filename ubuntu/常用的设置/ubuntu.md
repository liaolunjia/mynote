# 1、开启SSH
```bash
sudo apt-get install openssh-server
```

# 2、安装网络工具
```
sudo apt-get install net-tools
```

# 3、命令行参数提示工具
```
sudo apt-get install tldr
```

# 4、免密码SSh
## 4.1 主机输入ssh-keygen生成秘钥
```
ssh-keygen
```
## 4.2  一路enter
![图片1](pic/01.png)

## 4.3 将秘钥发送到从机
```bash
ssh-copy-id -i .ssh/id_rsa.pub rikirobot@192.168.2.119
```
![图片2](pic/02.png)

## 5 命令行快速打开clion
在`.bashrc` 设置
```bash
export PATH="/home/liao/.local/share/JetBrains/Toolbox/apps/CLion/ch-0/211.7628.27/bin/:$PATH"
```