//
// Created by liao on 2021/8/8.
//

#include "behaviortree_cpp_v3/bt_factory.h"

#include "test_node.h"


static const char* xml_text = R"(

<root main_tree_to_execute = "MainTree" >

    <BehaviorTree ID="MainTree">
        <Sequence>
            <SaySomething     message="Hi" />
            <SaySomething     message="Fuck" />
            <SaySomething     message="NMB" />
            <Sequence   name="SinS">
                <SaySomething     message="Hi2" />
                <SaySomething     message="Fuck2" />
                <SaySomething     message="NMB2" />
            </Sequence>

            <Run    name="run_name" />
            <Move    cmd_vel="4.0;3.0;3.14" />
            <SaySomething     message="11111111" />
        </Sequence>
    </BehaviorTree>

</root>
 )";


int main()
{

    BT::BehaviorTreeFactory factory;

    // ID与 <SaySomething     message="Hi" />要对应
    // 这意味着行为树这个节点绑定了这个实现方法
    factory.registerNodeType<TestNode::SaySomething>("SaySomething");
    factory.registerNodeType<TestNode::Run>("Run");
    factory.registerNodeType<TestNode::MoveAction>("Move");


    auto tree = factory.createTreeFromText(xml_text);

    tree.tickRoot();

    while (true)
    {
        SleepMS(10000);
        tree.tickRoot();
    }



    return 0;
}

