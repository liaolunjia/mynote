# ROS入门教程

文档撰写：廖伦稼
##  本次教程的软件版本
ubuntu: 20.04
ROS: noetic
CLion: 2021.1.1
catkin_tools   (note:本教程使用catkin_tools 而不是 catkin_make，听说catkin_tools更好用，所以我以后应该会用catkin_tools代替catkin_make)



##  参考资料
CLion的下载以及基本配置可参考[Install CLion](https://www.jetbrains.com/help/clion/installation-guide.html)  

catkin_tools的下载以及基本配置可参考[install catkin_tools](https://catkin-tools.readthedocs.io/en/latest/installing.html)  

```shell
# 在catkin 工作空间配置，不然编译时候一堆警告
catkin config --merge-devel
```


catkin下CMakeLists.txt编写教程： [catkin/CMakeLists.txt](http://wiki.ros.org/catkin/CMakeLists.txt)
##  开发流程
###  1、创建工作空间

```bash
source /opt/ros/noetic/setup.bash          # Source ROS noetic to use Catkin
mkdir -p ~/my_ros_ws/src            # Make a new workspace and source space
```
###  2、初始化工作空间

```bash
cd my_ros_ws/src/
catkin_init_workspace    # 创建顶层CMakeLists.txt文件，CLion利用此文件打开项目
```
###  3、创建两个rospackage

```bash
catkin create pkg pkg_a --catkin-deps roscpp   #有依赖
catkin create pkg pkg_b    #无依赖
```
###  4、编译一下各个rospackage

```bash
cd ~/my_ros_ws
catkin build
source devel/setup.bash # Load the workspace's environment
```
###  5、查看一下当前目录结构
在my_ros_ws 下有四个文件夹，分别存储源码和编译构建过程中生成的文件的，这里主要关注src文件夹，我们生成的ros包都在这个文件夹里面。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210520171154857.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NDE2MjU4,size_16,color_FFFFFF,t_70#pic_center)
进入src
这里可以看到我们刚才生成的两个rospackage，还有一个顶层的CmakeLists.txt,等一下CLion就是利用这个CmakeLists.txt打开整个ROS工作区
![在这里插入图片描述](https://img-blog.csdnimg.cn/2021052017151673.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NDE2MjU4,size_16,color_FFFFFF,t_70#pic_center)
###  6、打开CLion
这里我假设你已经安装好CLion
打开一个新的命令行窗口
```bash
cd my_ros_ws
source devel/setup.bash 
sh /opt/clion-*/bin/clion.sh  #打开CLion
```
打开CLion后选择CMakeLists.txt打开项目
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210520173027385.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NDE2MjU4,size_16,color_FFFFFF,t_70#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210520173228269.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NDE2MjU4,size_16,color_FFFFFF,t_70#pic_center)
###  7、 配置CLion
上一步打开CLion后应该会自动跳出项目向导，需要配置一下，如果没有跳出该画面，则可以手动打开 File | Settings | Build, Execution, Deployment | CMake

这里的CMake选项和构建目录要改一下，改成catkin能识别的，不然会指向另一个自动生成的构建目录。

```bash
-DCATKIN_DEVEL_PREFIX=../devel
../build
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210520174246887.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NDE2MjU4,size_16,color_FFFFFF,t_70#pic_center)  

不出意外的话，打开后可以看到如下界面和工程结构
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210520174921123.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NDE2MjU4,size_16,color_FFFFFF,t_70#pic_center)

###  8、运行一个小Demo
接下来运行两个ROS官方提供的[Demo](http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c++%29), 分别是talker 发布话题，listener 监听话题
####  8.1 创建两个CPP文件，分别命名为talker.cpp 、listener.cpp
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210520180023470.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NDE2MjU4,size_16,color_FFFFFF,t_70#pic_center)
talker.cpp

```cpp
#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>
//namespace talk_ns {
/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv) {
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "talker"); //talker is node name

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;

  /**
   * The advertise() function is how you tell ROS that you want to
   * publish on a given topic name. This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing. After this advertise() call is made, the master
   * node will notify anyone who is trying to subscribe to this topic name,
   * and they will in turn negotiate a peer-to-peer connection with this
   * node.  advertise() returns a Publisher object which allows you to
   * publish messages on that topic through a call to publish().  Once
   * all copies of the returned Publisher object are destroyed, the topic
   * will be automatically unadvertised.
   *
   * The second parameter to advertise() is the size of the message queue
   * used for publishing messages.  If messages are published more quickly
   * than we can send them, the number here specifies how many messages to
   * buffer up before throwing some away.
   */
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);

  ros::Rate loop_rate(10);

  /**
   * A count of how many messages we have sent. This is used to create
   * a unique string for each message.
   */
  int count = 0;
  while (ros::ok()) {
    /**
     * This is a message object. You stuff it with data, and then publish it.
     */
    std_msgs::String msg;

    std::stringstream ss;
    ss << "hello world " << count;
    msg.data = ss.str();

    ROS_INFO("%s", msg.data.c_str());

    /**
     * The publish() function is how you send messages. The parameter
     * is the message object. The type of this object must agree with the type
     * given as a template parameter to the advertise<>() call, as was done
     * in the constructor above.
     */
    chatter_pub.publish(msg);

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }

  return 0;
}
```
listener.cpp

```cpp
#include "ros/ros.h"
#include "std_msgs/String.h"

/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "listener");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;

  /**
   * The subscribe() call is how you tell ROS that you want to receive messages
   * on a given topic.  This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing.  Messages are passed to a callback function, here
   * called chatterCallback.  subscribe() returns a Subscriber object that you
   * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
   * object go out of scope, this callback will automatically be unsubscribed from
   * this topic.
   *
   * The second parameter to the subscribe() function is the size of the message
   * queue.  If messages are arriving faster than they are being processed, this
   * is the number of messages that will be buffered up before beginning to throw
   * away the oldest ones.
   */
  ros::Subscriber sub = n.subscribe("chatter", 1000, chatterCallback);

  /**
   * ros::spin() will enter a loop, pumping callbacks.  With this version, all
   * callbacks will be called from within this thread (the main one).  ros::spin()
   * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
   */
  ros::spin();

  return 0;
}
```
####  8.2 编写CmakeLists.txt文件
（！！！注意:这个CmakeLists.txt是pkg_a目录下的CmakeLists.txt，不是src目录下的CmakeLists.txt）
在CmakeLists.txt的末尾添加以下内容

```xml
add_executable(talker src/talker.cpp)
target_link_libraries(talker ${catkin_LIBRARIES})

add_executable(listener src/listener.cpp)
target_link_libraries(listener ${catkin_LIBRARIES})
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210520181314652.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NDE2MjU4,size_16,color_FFFFFF,t_70#pic_center)
####  8.3 编译CPP文件

```bash
cd ~/my_ros_ws
catkin build
```
####  8.4 运行talker 和 listener
打开3个命令行窗口

命令行窗口一运行roscore

```bash
roscore
```
命令行窗口二运行listener

```bash
cd my_ros_ws/
source devel/setup.bash 
rosrun pkg_a listener
```
命令行窗口三运行talker

```bash
cd my_ros_ws/
source devel/setup.bash 
rosrun pkg_a talker
```
不出意外的话，可以看到两个结点运行成功
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210520182511474.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM3NDE2MjU4,size_16,color_FFFFFF,t_70#pic_center)
