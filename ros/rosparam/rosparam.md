# ros 参数服务器的使用

## 1、在launch文件载入param
### 直接设置param
```xml
<launch>

	<!-- 直接设置param -->
	<param name="param1" value="1" />
	<param name="param2" value="2" />

	<!--以下写法将参数转成YAML文件加载，注意param前面必须为空格，不能用Tab，否则YAML解析错误-->
	<rosparam>   
        param3: 3
        param4: 4
        param5: 5
    </rosparam>

	<!-- 把关节控制的配置信息读取到参数服务器 -->
    <rosparam file="$(find rosparam)/demo/controllers.yaml" command="load"/>

</launch>
```


### 加载URDF文件

```xml
<launch>
	<!-- cpmmand =“$(find xacro)/xacro  xacro描述文件  输入xacro文件的参数 ” -->
	<param name="robot_description" command="$(find xacro)/xacro $(find robot_pkg)/urdf/robot.xacro
			load_chassis:=$(arg load_chassis)
			load_gimbal:=$(arg load_gimbal)
			load_shooter:=$(arg load_shooter)
			use_simulation:=$(arg use_simulation)
			use_rm_gazebo:=$(arg use_rm_gazebo)
			roller_type:=$(arg roller_type)"
	/>
</launch>
```

## 2、在C++中使用param  
###  get param 三种方法
```cpp
	int parameter1, parameter2, parameter3, parameter4, parameter5;

	//① ros::param::get()获取参数“param1”的value，写入到parameter1上
	bool ifget1 = ros::param::get("param1", parameter1);
	
	//② ros::NodeHandle::getParam()获取参数，与①作用相同
	bool ifget2 = nh.getParam("param2",parameter2);
	
	//③ ros::NodeHandle::param()类似于①和②
	//但如果get不到指定的param，它可以给param指定一个默认值(如33333)
    nh.param("param3", parameter3, 33333);
	//parameter3 = nh.param("param3", 33333); 	//这两种方式都可以获取参数
```

### set param 两种方法
```cpp
	//① ros::param::set()设置参数
	parameter4 = 4;
	ros::param::set("param4", parameter4);

	//② ros::NodeHandle::setParam()设置参数
	parameter5 = 5;
	nh.setParam("param5",parameter5);
```


### check param 两种方法
```cpp
	//① ros::NodeHandle::hasParam()
	bool ifparam5 = nh.hasParam("param5");

	//② ros::param::has()
	bool ifparam6 = ros::param::has("param6");

```

### delete param 两种方法
```cpp
	//① ros::NodeHandle::deleteParam()
	bool ifdeleted5 = nh.deleteParam("param5");

	//② ros::param::del()
	bool ifdeleted6 = ros::param::del("param6");
```

## 3、在python中使用param
```python
#!/usr/bin/env python
# coding:utf-8

import rospy

def param_demo():
    rospy.init_node("param_demo")
    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        #get param
        parameter1 = rospy.get_param("/param1")
        parameter2 = rospy.get_param("/param2", default=222)
        rospy.loginfo('Get param1 = %d', parameter1)
        rospy.loginfo('Get param2 = %d', parameter2)

        #delete param
        rospy.delete_param('/param2')

        #set param
        rospy.set_param('/param2',2)
        
        #check param
        ifparam3 = rospy.has_param('/param3')
        if(ifparam3):
            rospy.loginfo('/param3 exists')
        else:
            rospy.loginfo('/param3 does not exist')

        #get all param names
        params = rospy.get_param_names()
        rospy.loginfo('param list: %s', params)

        rate.sleep()

if __name__=="__main__":
    param_demo()
```