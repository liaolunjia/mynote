# AMCL定位
## launch demo
```xml
<?xml version="1.0"?>
<launch>
    <!-- amcl node -->
    <node pkg="amcl" type="amcl" name="amcl" output="screen">

        <!--=========整体过滤参数==========================-->
        <!-- 允许的最小粒子数-->
        <param name="min_particles" value="100"/>
        <!-- 允许的最大粒子数-->
        <param name="max_particles" value="5000"/>
        <!-- 真实分布和估计分布之间的最大误差。-->
        <param name="kld_err" value="0.01"/>
        <!-- (1 - p) 的上标准正态分位数，其中 p 是估计分布的误差小于kld_err 的概率。-->
        <param name="kld_z" value="0.99"/>

        <!--使用位置估计滤波器前需要移动一段距离。也就是需要动一下才能判断出自己在哪里，单位米-->
        <param name="update_min_d" value="0.2"/>
        <!--使用位置估计滤波器前需要旋转运动-->
        <param name="update_min_a" value="0.5"/>

        <!--重采样前所需的过滤器更新次数-->
        <param name="resample_interval" value="2"/>
        <!--发布的TF的有效时间，用于将来判定这个TF还能不能用-->
        <param name="transform_tolerance" value="0.1"/>

        <!--慢速平均权重滤波器的指数衰减率，用于通过添加随机姿势决定何时恢复。-->
        <param name="recovery_alpha_slow" value="0.001"/>
        <!--快速平均权重滤波器的指数衰减率，用于通过添加随机姿势决定何时恢复。-->
        <param name="recovery_alpha_fast" value="0.1"/>

        <!--初始姿态均值，用于用高斯分布初始化滤波器。-->
        <!--初始姿态均值 (x) ，单位米-->
        <param name="initial_pose_x" value="0.0"/>
        <!--初始姿态均值 (y) ，单位米-->
        <param name="initial_pose_y" value="0.0"/>
        <!--初始姿态均值 (yaw) ，单位弧度-->
        <param name="initial_pose_a" value="0.0"/>
        <!--初始姿态协方差 (x*x) ，单位：0.5米*0.5米-->
        <param name="initial_cov_xx" value="0.25"/>
        <!--初始姿态协方差 (y*y)) ，单位：0.5米*0.5米-->
        <param name="initial_cov_yy" value="0.25"/>
        <!--初始姿态协方差 (yaw*yaw)) ，单位：弧度*弧度-->
        <param name="initial_cov_aa" value="0.068"/>


        <!--为可视化发布sacn和paths的最大速率 (Hz)，-1.0 禁用。-->
        <param name="gui_publish_rate" value="10"/>
        <!--在变量 ~initial_pose_* 和 ~initial_cov_* 中将最后估计的姿态和协方差存储到参数服务器的最大速率 (Hz)。此保存的姿势将在后续运行中用于初始化过滤器。-1.0 禁用。-->
        <param name="save_pose_rate" value="0.5"/>


        <!--设置为 true 时，AMCL 将订阅地图主题，而不是通过服务调用来接收其地图。-->
        <param name="use_map_topic" value="true"/>
        <!--当设置为 true 时，AMCL 将只使用它订阅的第一个地图，而不是在每次收到新地图时更新。-->
        <param name="first_map_only" value="true"/>

        <!--设置为 true 时，将在不需要时降低重采样率并帮助避免粒子剥夺。仅当有效粒子数（N_eff = 1/(sum(k_i^2))）低于当前粒子数的一半时，才会进行重采样。-->
        <param name="selective_resampling" value="false"/>


        <!--==================== 激光模型参数==============================-->
        <!--要考虑的最小扫描范围；-1.0 将导致使用激光报告的最小范围。-->
        <param name="laser_min_range" value="1"/>
        <!--要考虑的最大扫描范围；-1.0 将导致使用激光报告的最大射程。-->
        <param name="laser_max_range" value="5"/>
        <!--更新滤波器时，每次扫描中使用多少均匀间隔的光束。-->
        <param name="laser_max_beams" value="30"/>
        <!--模型 z_hit 部分的混合权重。-->
        <param name="laser_z_hit" value="0.95"/>
        <!--模型 z_short 部分的混合权重。-->
        <param name="laser_z_short" value="0.1"/>
        <!--模型 z_max 部分的混合权重。-->
        <param name="laser_z_max" value="0.05"/>
        <!--模型 z_rand 部分的混合权重。-->
        <param name="laser_z_rand" value="0.05"/>
        <!--模型的 z_hit 部分中使用的高斯模型的标准偏差。单位米-->
        <param name="laser_sigma_hit" value="0.2"/>
        <!--模型 z_short 部分的指数衰减参数。-->
        <param name="laser_lambda_short" value="0.1"/>
        <!--在地图上进行障碍物膨胀的最大距离，用于似然场模型。单位米-->
        <param name="laser_likelihood_max_dist" value="2"/>
        <!--使用哪个模型，beam，likelihood_field，likelihood_field_prob-->
        <param name="laser_model_type" value="likelihood_field"/>


        <!-- ================里程计模型参数==============-->
        <!--使用哪种模型，"diff"、"omni"、"diff-corrected"或"omni-corrected"。-->
        <param name="odom_model_type" value="diff"/>
        <!--根据机器人运动的旋转分量指定里程计‘旋转’估计中的预期噪声。-->
        <param name="odom_alpha1" value="0.2"/>
        <!--根据机器人运动的平移分量指定里程计‘旋转’估计中的预期噪声。-->
        <param name="odom_alpha2" value="0.2"/>
        <!--根据机器人运动的平移分量指定里程计‘平移’估计中的预期噪声。-->
        <param name="odom_alpha3" value="0.2"/>
        <!--根据机器人运动的旋转分量指定里程计‘平移’估计中的预期噪声。-->
        <param name="odom_alpha4" value="0.2"/>
        <!--与平移相关的噪声参数（仅在模型为"omni" 时使用）。-->
        <param name="odom_alpha5" value="0.2"/>
        <!--里程计坐标系的名字。-->
        <param name="odom_frame_id" value="odom"/>
        <!--机器人坐标系的名字。-->
        <param name="base_frame_id" value="base_link"/>
        <!--定位系统发布的坐标系名称-->
        <param name="global_frame_id" value="map"/>
        <!--将此设置为false以防止 amcl 发布全局框架和里程计框架之间的转换。-->
        <param name="tf_broadcast" value="true"/>
    </node>

</launch>

```