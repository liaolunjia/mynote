# 利用opencv绘图  

```cpp
//
// Created by liao on 2021/8/12.
//


//! [includes]
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include<opencv2/opencv.hpp>

#include <iostream>

const int map_rows = 508;   // 地图大小
const int map_cols = 868;   // 地图大小

int main()
{


    cv::Mat map(map_rows, map_cols, CV_8UC3,cv::Scalar(0, 0, 0));

    // 外围
    std::vector<cv::Point > contour_around;
    contour_around.emplace_back(cv::Point(30,30));
    contour_around.emplace_back(cv::Point(837, 30));
    contour_around.emplace_back(cv::Point(837, 477));
    contour_around.emplace_back(cv::Point(30, 477));
//    cv::polylines(map, contour_around, true, cv::Scalar(255, 255, 255), 2, cv::LINE_AA);
    cv::fillPoly(map, contour_around, cv::Scalar(255, 255, 255));


    std::vector<std::vector<cv::Point >> contours;

    std::vector<cv::Point > contour_B5;
    contour_B5.emplace_back(cv::Point(434,271));
    contour_B5.emplace_back(cv::Point(451, 254));
    contour_B5.emplace_back(cv::Point(434, 237));
    contour_B5.emplace_back(cv::Point(417, 254));
    contours.push_back(contour_B5);

    std::vector<cv::Point > contour_B9;
    contour_B9.emplace_back(cv::Point(30,130));
    contour_B9.emplace_back(cv::Point(129, 130));
    contour_B9.emplace_back(cv::Point(129, 149));
    contour_B9.emplace_back(cv::Point(30, 149));
    contours.push_back(contour_B9);


    std::vector<cv::Point > contour_B1;
    contour_B1.emplace_back(cv::Point(738,358));
    contour_B1.emplace_back(cv::Point(837, 358));
    contour_B1.emplace_back(cv::Point(837, 377));
    contour_B1.emplace_back(cv::Point(738, 377));
    contours.push_back(contour_B1);


    std::vector<cv::Point > contour_B3;
    contour_B3.emplace_back(cv::Point(668,30));
    contour_B3.emplace_back(cv::Point(687, 30));
    contour_B3.emplace_back(cv::Point(687, 129));
    contour_B3.emplace_back(cv::Point(668, 129));
    contours.push_back(contour_B3);

    std::vector<cv::Point > contour_B7;
    contour_B7.emplace_back(cv::Point(180,378));
    contour_B7.emplace_back(cv::Point(199, 378));
    contour_B7.emplace_back(cv::Point(199, 477));
    contour_B7.emplace_back(cv::Point(180, 477));
    contours.push_back(contour_B7);

    std::vector<cv::Point > contour_B6;
    contour_B6.emplace_back(cv::Point(384,123));
    contour_B6.emplace_back(cv::Point(483, 123));
    contour_B6.emplace_back(cv::Point(483, 142));
    contour_B6.emplace_back(cv::Point(384, 142));
    contours.push_back(contour_B6);


    std::vector<cv::Point > contour_B4;
    contour_B4.emplace_back(cv::Point(384,364));
    contour_B4.emplace_back(cv::Point(483, 364));
    contour_B4.emplace_back(cv::Point(483, 383));
    contour_B4.emplace_back(cv::Point(384, 383));
    contours.push_back(contour_B4);

    std::vector<cv::Point > contour_B8;
    contour_B8.emplace_back(cv::Point(180,244));
    contour_B8.emplace_back(cv::Point(259, 244));
    contour_B8.emplace_back(cv::Point(259, 263));
    contour_B8.emplace_back(cv::Point(180, 263));
    contours.push_back(contour_B8);

    std::vector<cv::Point > contour_B2;
    contour_B2.emplace_back(cv::Point(609,244));
    contour_B2.emplace_back(cv::Point(688, 244));
    contour_B2.emplace_back(cv::Point(688, 263));
    contour_B2.emplace_back(cv::Point(609, 263));
    contours.push_back(contour_B2);


    cv::fillPoly(map, contours, cv::Scalar(0, 0, 0));

    imshow("Display window", map);
    imwrite("../map.png", map);
    cv::waitKey(0); // Wait for a keystroke in the window

    return 0;
}
```