# git使用笔记
---
## 基本三大命令
```bash
git add .
git commit -m "commit data"
git push
```

---
## Git全局设置
```bash
git config --global user.name "廖伦稼"
git config --global user.email "809525028@qq.com"
```

## 创建一个新仓库
```bash
git clone https://gitlab.com/liaolunjia/mynote.git
cd mynote
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

## 推送现有文件夹
```bash
cd existing_folder
git init
git remote add XXX https://gitlab.com/liaolunjia/XXX.git  # 别名
# git remote add mynote https://gitlab.com/gdut-yxj/tutorials/ros/
git add .
git commit -m "Initial commit"
git push -u mynote master
```

## 推送现有的Git仓库
```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/BugLiao/mynote.git
git push -u origin --all
git push -u origin --tags
```

##  .gitignore 不起作用

```bash
git rm -r --cached .
```
