/*******************************************************************************
 * Copyright (c) 2022/3/19, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <iostream>
#define PORT 9734

int main() {
  /* create a socket */
  int server_sockfd = socket(AF_INET, SOCK_STREAM, 0);

  struct sockaddr_in server_addr {};
  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
  server_addr.sin_port = htons(PORT);

  /* bind with the local file */
  bind(server_sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));

  /* listen */
  listen(server_sockfd, 5);

  char ch;
  int client_sockfd;
  struct sockaddr_in client_addr {};
  socklen_t len = sizeof(client_addr);
  while (true) {
    printf("server waiting:\n");

    /* accept a connection , accept 会阻塞*/
    client_sockfd = accept(server_sockfd, (struct sockaddr *)&client_addr, &len);

    /* exchange data */
    recv(client_sockfd, &ch, 1, 0);
    std::cout << "get char from client: " << ch << std::endl;
    ++ch;
    send(client_sockfd, &ch, 1, 0);

    /* close the socket */
    close(client_sockfd);
  }

  return 0;
}