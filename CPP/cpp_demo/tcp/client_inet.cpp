/*******************************************************************************
 * Copyright (c) 2022/3/19, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cstdlib>
#include <iostream>

#define PORT 9734

int main() {
  /* create a socket */
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);

  struct sockaddr_in address {};
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = inet_addr("127.0.0.1");  // 127.0.0.1 本地回环地址
  address.sin_port = htons(PORT);

  /* connect to the server */
  int result = connect(sockfd, (struct sockaddr *)&address, sizeof(address));
  if (result == -1) {
    std::cout << "connect failed: " << std::endl;
    exit(1);
  }

  /* exchange data */
  char ch = 'A';
  send(sockfd, &ch, 1, 0);
  recv(sockfd, &ch, 1, 0);
  std::cout << "get char from server: " << ch << std::endl;

  /* close the socket */
  close(sockfd);

  return 0;
}