# socket

recv/send 通常应用于TCP

recvfrom/sendto通常应用于UDP。

```cpp
int socket(int domain, int type, int protocol)
```

## 地址域 (domains)

`AF_UNIX`表示以文件方式创建socket

`AF_INET`表示以端口方式创建socket

不同的地址域有自己的地址格式

### AF_UNIX

一般是同一台电脑的不同进程通信

这种方式是通过文件来将服务器和客户端连接起来的

### AF_INET

一般是网络上的不同电脑之间的通信

## socket类型(types)

`SOCK_STREAM` - 常用于TCP

`SOCK_DGRAM` - 常用于UDP

### SOCK_STREAM

stream sockets 将通信视为连续的字符流。

表示创建一个有序的，可靠的，面向连接的socket，因此如果我们要使用TCP，就应该指定为SOCK_STREAM

### SOCK_DGRAM

datagram sockets 必须一次读取整个消息

表示创建一个不可靠的，无连接的socket，因此如果我们要使用UDP，就应该指定为SOCK_DGRAM

## protocol

指定socket的协议类型，我们一般指定为0表示由第一第二两个参数自动选择。

---

# 地址格式

```cpp
struct sockaddr_un  
{  
  sa_family_t sun_family;  /* AF_UNIX */  
  char sun_path[];         /* pathname */  
}  
  
  
struct sockaddr_in  
{  
  short int sin_family;          /* AF_INET */  
  unsigned short int sin_port;   /* port number */  
  struct in_addr sin_addr;       /* internet address */  
}  
```

其中in_addr正是用来描述一个ip地址的：

```cpp
struct in_addr  
{  
unsigned long int s_addr;  
}  
```

sun_path存放socket的本地文件名，sin_addr存放socket的ip地址，sin_port存放socket的端口号。

