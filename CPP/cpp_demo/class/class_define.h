/*******************************************************************************
 * Copyright (c) 2022/3/20, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#pragma once
#include <iostream>
class Base {
 public:
  Base() = default;
  void sayHi() { std::cout << "HI, I`m Base" << std::endl; }
  virtual void sayHello() { std::cout << "Hello, I`m Base" << std::endl; }
};

class ClassA : public Base {
 public:
  ClassA() = default;
  void sayHi() { std::cout << "HI, I`m A" << std::endl; }
  virtual void sayHello() override { std::cout << "Hello, I`m A" << std::endl; }
  void saySomeThing(int &) { std::cout << "The number is int" << std::endl; }
  void saySomeThing(double &) { std::cout << "The number is double" << std::endl; }
};