/*******************************************************************************
 * Copyright (c) 2022/3/19, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <iostream>

#include "common.h"
#include "header.pb.h"
#include "robot.pb.h"
constexpr int SERV_PORT = 8000;
constexpr int BUFFLEN = 100;
int main() {
  /* socket文件描述符 */
  int sock_fd;
  /* 设置address */
  struct sockaddr_in server {};
  struct sockaddr_in client {};

  /* 建立udp socket */
  if ((sock_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    std::cout << "socket error" << std::endl;
    exit(1);
  };

  server.sin_family = AF_INET;
  server.sin_port = htons(SERV_PORT);
  server.sin_addr.s_addr = htonl(INADDR_ANY);  // IP地址，需要进行网络序转换，INADDR_ANY：本地地址

  /* 绑定socket */
  if (bind(sock_fd, (struct sockaddr *)&server, sizeof(server)) < 0) {
    std::cout << "bind error" << std::endl;
    exit(1);
  }

  int recv_num;
  char recv_buf[BUFFLEN];

  while (true) {
    std::cout << "========================" << std::endl;
    socklen_t client_address_size = sizeof(client);
    recv_num = recvfrom(sock_fd, &recv_buf, 100, 0, (struct sockaddr *)&client, &client_address_size);
    if (recv_num < 0) {
      std::cout << "recvfrom error" << std::endl;
      exit(1);
    }

    proto_comm::HeaderParse header_parse;
    if (header_parse.ParseFromArray(recv_buf, recv_num)) {
      std::cout << "header succcess" << std::endl;
    } else {
      std::cout << "header fail" << std::endl;
    }

    if (header_parse.header().data_type() == CMD_VEL) {
      std::cout << "is CMD_VEL" << std::endl;
      robot::CmdVel cmd_vel;
      cmd_vel.ParseFromArray(recv_buf, recv_num);
      std::cout << "cmd_vel header.type() ：" << cmd_vel.header().data_type() << std::endl;
      std::cout << "cmd_vel header.frame_id() ：" << cmd_vel.header().frame_id() << std::endl;
      std::cout << "cmd_vel x() ：" << cmd_vel.x() << std::endl;
      std::cout << "cmd_vel y() ：" << cmd_vel.y() << std::endl;
      std::cout << "cmd_vel z() ：" << cmd_vel.z() << std::endl;
    }

    if (header_parse.header().data_type() == STATE) {
      std::cout << "is STATE" << std::endl;
      robot::State state;
      state.ParseFromArray(recv_buf, recv_num);
      std::cout << "State header.type() ：" << state.header().data_type() << std::endl;
      std::cout << "State header.frame_id() ：" << state.header().frame_id() << std::endl;
      std::cout << "State x() ：" << state.x() << std::endl;
      std::cout << "State y() ：" << state.y() << std::endl;
      std::cout << "State z() ：" << state.z() << std::endl;
    }

    if (header_parse.header().data_type() == IMU) {
      std::cout << "is IMU" << std::endl;
      robot::Imu imu;
      imu.ParseFromArray(recv_buf, recv_num);
      std::cout << "IMU header.type() ：" << imu.header().data_type() << std::endl;
      std::cout << "IMU header.frame_id() ：" << imu.header().frame_id() << std::endl;
      std::cout << "IMU roll ：" << imu.roll() << std::endl;
      std::cout << "IMU pitch ：" << imu.pitch() << std::endl;
      std::cout << "IMU yaw ：" << imu.yaw() << std::endl;
    }
  }

  close(sock_fd);

  return 0;
}