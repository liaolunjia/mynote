//
// Created by liao on 2021/8/11.
//

#include <iostream>
#include "foo.h"


namespace Foo
{
    void TestLib::display() {
        std::cout<<"First display3323"<<std::endl;
    }

    void TestLib::display(int a) {
        std::cout<<"Second display:"<<a<<std::endl;
    }

} // end namespace Foo