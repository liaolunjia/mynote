# 平衡车动力学分析

## 参数符号

| 名字           | 意义               | 值        |
|:-------------:|:------------------:|:--------:|
| M             | 车体质量            |2.0 kg|
| m             | 轮子质量            |0.267401 kg|
| L             | 车体质心到车轴中心的距离 |0.2 m|
| $I_\omega$    | 车轮旋转惯量         |0.00080889 $kg.m^2$|
| $I_\alpha$    | 车体对 pitch 轴的旋转惯量 |0.08 $kg.m^2$|
| $I_\delta$    | 车体对 yaw 轴的旋转惯量 ||
| R             | 车轮半径 |0.055 m   |
| D             | 轮距 |0.328 m|
| $\theta$      | 车体与 Z 轴的夹角（倾角）pitch|
| $\delta$      | 车体与 X 轴的夹角 (偏角) yaw|
| $C_l , C_r$   | 左右轮输出力矩 ||
| $x_l , x_r, x, x_p$   | 左轮，右轮，轴心，本体在X轴位移 ||
| $z_p$   |本体 在Z轴位移 ||
| $f_l , f_r$   | 左右轮与地面的摩擦力 ||
| $H_l , H_r$   | 左右轮在水平方向给车体的力 ||
| $V_l , V_r$   | 左右轮在垂平方向给车体的力 ||
---

# 建模思路
#### 对左轮进行受力分析：
$$
m \cdot \ddot{x_l} =  f_l - H_l \tag{1} 
$$

$$
I_\omega \cdot \frac{\ddot{x_l}}{R} = C_l - f_l \cdot R \tag{2}
$$

联立（1）、（2）得:
$$
\ddot{x_l} = \frac{C_lR -  H_lR^2}{I_\omega + mR^2} \tag{3}
$$


同理得右轮的方程：
$$
\ddot{x_r} = \frac{C_rR -  H_rR^2}{I_\omega + mR^2} \tag{4}
$$


联立（1）（2）（3）（4）得：
$$
\begin{split}
H_l + H_r &= \frac{1}{R}（C_l + C_r）- \frac{I_\omega + R^2}{R^2}（\ddot{x_l} + \ddot{x_r}）\\
&= \frac{1}{R}（C_l + C_r）- \frac{I_\omega + R^2}{R^2} \cdot 2 \cdot\ddot{x}
\tag{4.1}
\end{split}
$$
#### 对车身进行受力分析：

在$x$方向的力：
$$
M \cdot \ddot{x_p} = H_l + H_r \tag{5}
$$

在$pitch$轴向的合外力矩：
$$
I_\theta \cdot \ddot{\theta} = ( V_l +  V_r) \cdot L \cdot \sin \theta - ( H_l +  H_r) \cdot L \cdot \cos \theta - (C_l + C_r)   \tag{6}
$$

$$
M \cdot \ddot{z_p} = V_l +  V_r - M \cdot g  \tag{7}
$$

在$yaw$轴向的合外力矩：
$$
I_\delta \cdot \ddot{\delta} = \frac{D}{2} \cdot ( H_r -  H_l) \tag{8}
$$

#### 运动学分析 ：
轴心运动学分析：
$$
x = \frac{x_l + x_r}{2} \tag{9} 
$$

车体运动学分析：
$$
x_p = x + L \sin \theta   \tag{10} 
$$

$$
z_p = L  \cos \theta \tag{11} 
$$

$$
\dot{\delta} = (\dot{x_r} - \dot{x_l}) \cdot \frac{1}{D} \tag{12} 
$$

<!-- #### 由（10）得 :
$$
\dot{x_p} =  \frac{1}{2}(\dot{x_l} + \dot{x_r}) + L \dot{\theta}  \cos \theta\\
\ddot{x_p}  =  \frac{1}{2}(\ddot{x_l} + \ddot{x_r}) + L \ddot{\theta}  \cos \theta - L \dot{\theta}^2  \sin \theta \tag{13} 
$$

#### 由（11）得 :
$$
\dot{z_p} = - L \dot{\theta}  \sin \theta \\
\ddot{z_p} = - L \ddot{\theta}  \sin \theta - L \dot{\theta}^2  \cos \theta \tag{14} 
$$ -->

<!-- #### 由（12）得 :
$$
\ddot{\delta} = (\ddot{x_r} - \ddot{x_l}) \cdot \frac{1}{D} \tag{15} 
$$ -->

# 1、 get  $\ddot{x}$

由 （9）得：
$$
x = \frac{1}{2}(x_l + x_r) 
$$

$$
\dot{x} = \frac{1}{2}(\dot{x_l} + \dot{x_r})
$$

$$
\ddot{x} = \frac{1}{2}(\ddot{x_l} + \ddot{x_r}) \tag{13} 
$$

将（3）、（4）代入（13）得：
$$
\begin{split}
\ddot{x} &= \frac{1}{2}(\ddot{x_l} + \ddot{x_r})\\
&=\frac{1}{2}(\frac{C_lR -  H_lR^2}{I_\omega + mR^2} + \frac{C_rR -  H_rR^2}{I_\omega + mR^2}) \\
&=\frac{1}{2}(\frac{C_lR + C_rR - ( H_l +  H_r)R^2}{I_\omega + mR^2} ) \\
代入（5）:
&=\frac{1}{2}(\frac{C_lR + C_rR -M\ddot{x_p}R^2}{I_\omega + mR^2}) \\
\tag{14}
\end{split} 
$$

由 （10）得：
$$
x_p = x + L \sin \theta  
$$

$$
\dot{x_p} =  \dot{x}  + L \dot{\theta}  \cos \theta\\
$$

$$
\ddot{x_p}  =  \ddot{x}  + L \ddot{\theta}  \cos \theta - L \dot{\theta}^2  \sin \theta \tag{15} 
$$

将（15）代入（14）得：
$$
\ddot{x} = \frac{1}{2}(\frac{C_lR + C_rR -MR^2 \cdot( \ddot{x}  + L \ddot{\theta}  \cos \theta - L \dot{\theta}^2  \sin \theta)}{I_\omega + mR^2} ) \\
$$

$$
(2I_\omega + 2mR^2)\ddot{x} = C_lR + C_rR -MR^2 \cdot( \ddot{x}  + L \ddot{\theta}  \cos \theta - L \dot{\theta}^2  \sin \theta)  \\
$$

$$
(2I_\omega + 2mR^2 + MR^2)\ddot{x} + MR^2L \ddot{\theta}  \cos \theta  - MR^2L \dot{\theta}^2  \sin \theta = C_lR + C_rR   \\
$$

$$
(\frac{2I_\omega}{R^2} + 2m + M)\ddot{x} + ML \ddot{\theta}  \cos \theta  - ML \dot{\theta}^2  \sin \theta = \frac{1}{R}(C_l + C_r)   \tag{16}
$$


# 2、 get  $\ddot{\theta}$
由（11）得：

$$
z_p =  L  \cos \theta \\
\dot{z_p} = - L \dot{\theta}  \sin \theta \\
$$ 

$$
\ddot{z_p} = - L \ddot{\theta}  \sin \theta - L \dot{\theta}^2  \cos \theta \tag{17} 
$$

<!-- 由（6）得：
$$
\begin{split}
I_\theta \cdot \ddot{\theta} &= ( V_l +  V_r) \cdot L \cdot \sin \theta - ( H_l +  H_r) \cdot L \cdot \cos \theta - (C_l + C_r) \\
代入（7）(5)：&= M（\ddot{z_p}+g） \cdot L \cdot \sin \theta - M \ddot{x_p} \cdot L \cdot \cos \theta - (C_l + C_r) \\
代入(17)（15)：&= M（- L \ddot{\theta}  \sin \theta - L \dot{\theta}^2  \cos \theta + g） \cdot L \cdot \sin \theta - M (\ddot{x}  + L \ddot{\theta}  \cos \theta - L \dot{\theta}^2  \sin \theta )\cdot L \cdot \cos \theta - (C_l + C_r) \\
&= - ML^2 \ddot{\theta}  \sin^2 \theta - ML^2 \dot{\theta}^2  \cos \theta \sin \theta+ MgL \sin \theta  - M (\ddot{x}  + L \ddot{\theta}  \cos \theta - L \dot{\theta}^2  \sin \theta )\cdot L \cdot \cos \theta - (C_l + C_r) \\
\end{split}
\tag{18}
$$ -->

由（6）得：
$$
\begin{split}
I_\theta \cdot \ddot{\theta} &= ( V_l +  V_r) \cdot L \cdot \sin \theta - ( H_l +  H_r) \cdot L \cdot \cos \theta - (C_l + C_r) \\
代入（7）(5)：&= M（\ddot{z_p}+g） \cdot L \cdot \sin \theta - M \ddot{x_p} \cdot L \cdot \cos \theta - (C_l + C_r) \\
代入(17)（15)：&= M（- L \ddot{\theta}  \sin \theta - L \dot{\theta}^2  \cos \theta + g） \cdot L \cdot \sin \theta - M (\ddot{x}  + L \ddot{\theta}  \cos \theta - L \dot{\theta}^2  \sin \theta )\cdot L \cdot \cos \theta - (C_l + C_r) \\
&= - ML^2 \ddot{\theta}  \sin^2 \theta - ML^2 \dot{\theta}^2  \cos \theta \sin \theta+ MgL \sin \theta  - M (\ddot{x}  + L \ddot{\theta}  \cos \theta - L \dot{\theta}^2  \sin \theta )\cdot L \cdot \cos \theta - (C_l + C_r) \\
\end{split}
\tag{18}
$$

# 3、 get  $\ddot{\delta}$
联立(8)、(3)、(4)、(15)得：

$$
\begin{split}
I_\delta \cdot \ddot{\delta} &= \frac{D}{2} \cdot ( H_r -  H_l) \\
& = \frac{D}{2} \cdot ( \frac{C_rR - \ddot{x_r} (I_{\omega}+mR^2)}{R^2} - \frac{C_lR - \ddot{x_l} (I_{\omega}+mR^2)}{R^2} ) \\
& = \frac{D}{2} \cdot ( \frac{C_rR - C_lR }{R^2} - \frac{\ddot{x_r} (I_{\omega}+mR^2) - \ddot{x_l} (I_{\omega}+mR^2)}{R^2} ) \\
& = \frac{D}{2} \cdot ( \frac{C_r - C_l }{R} - \frac{(\ddot{x_r} -  \ddot{x_l})(I_{\omega}+mR^2)}{R^2} ) \\
& = \frac{D}{2} \cdot ( \frac{C_r - C_l }{R} - \frac{\ddot{\delta}D(I_{\omega}+mR^2)}{R^2} ) \\
\tag{19}
\end{split}
$$

整理得：
$$
(\frac{2I_{\delta} }{D} + \frac{DI_{\omega}}{R^2} + Dm) \cdot \ddot{\delta} =  \frac{1}{R}(C_r - C_l) \tag{20}
$$


# 模型线性化
在平衡点附近对系统进行线性化处理，因为在平衡点附近 $\theta \approx 0$，那么$\sin \theta \approx \theta, \cos \approx 1$,因此可以将（16）、（18）、（20）线性化得：

$$
(\frac{2I_\omega}{R^2} + 2m + M)\ddot{x} + ML \ddot{\theta}  = \frac{1}{R}(C_l + C_r)   \tag{21}
$$

$$
I_\theta \cdot \ddot{\theta} = MgL\theta - M L\ddot{x} -ML^2\ddot{\theta} - (C_l + C_r)
\tag{22}
$$

$$
(\frac{2I_{\delta} }{D} + \frac{DI_{\omega}}{R^2} + Dm) \cdot \ddot{\delta} =  \frac{1}{R}(C_r - C_l) \tag{23}
$$
# 状态方程

$$
% X_dot
\begin{bmatrix}
\ddot{x} \\
\ddot{\delta} \\
\dot{\theta} \\
\ddot{\theta} \\
\end{bmatrix} =
% A
\begin{bmatrix}
a_{11} & a_{14} & a_{14} & a_{14} \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 1 \\
a_{41} & a_{42} & a_{43} & a_{44} \\
\end{bmatrix} \cdot 
% X
\begin{bmatrix}
\dot{x} \\
\dot{\delta} \\
\theta \\
\dot{\theta} \\
\end{bmatrix} + 
% B
\begin{bmatrix}
b_{11}  & b_{12} \\
b_{21}  & b_{22} \\
0  & 0 \\
b_{41}  & b_{42} \\
\end{bmatrix} \cdot 
% U
\begin{bmatrix}
C_l \\
C_r \\
\end{bmatrix} 
$$

$$
b_{21} = - \frac{DR}{2I_{\delta}R^2 + D^2I_{\omega} + D^2mR^2}
$$

$$
b_{22} = \frac{DR}{2I_{\delta}R^2 + D^2I_{\omega} + D^2mR^2}
$$